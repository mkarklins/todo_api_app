class Api::V1::TasksController < ApplicationController
  before_action :set_task, only: [:destroy, :update]

  def index
    tasks = Task.all.includes(:tags)

    render json: tasks
  end

  def create
    task = Task.new(task_params)

    if task.save_with_tags(task_tags)
      render json: task
    else
      render json: task, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  def update
    @task.assign_attributes(task_params)

    if @task.save_with_tags(task_tags)
      render json: @task
    else
      render json: @task, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  def destroy
    if @task.destroy
      head 204
    else
      render json: @task, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title])
  end

  def task_tags
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:tags]).fetch(:tags, [])
  end
end
