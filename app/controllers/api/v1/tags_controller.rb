class Api::V1::TagsController < ApplicationController
  before_action :set_tag, only: [:destroy, :update]

  def index
    tags = Tag.all

    render json: tags
  end

  def create
    tag = Tag.new(tag_params)

    if tag.save
      render json: tag
    else
      render json: tag, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  def update
    if @tag.update_attributes(tag_params)
      render json: @tag
    else
      render json: @tag, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  def destroy
    if @tag.destroy
      head 204
    else
      render json: @tag, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  private

  def set_tag
    @tag = Tag.find(params[:id])
  end

  def tag_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title])
  end
end
