class Task < ApplicationRecord
  validates :title, presence: true, length: { maximum: 255 }

  has_and_belongs_to_many :tags, dependent: :destroy

  def save_with_tags(tags)
    raise ArgumentError.new("`tags` must be an array") unless tags.is_a?(Array)

    return false unless valid?

    transaction do
      self.tags = find_or_initialize_tags(tags)
      save
    end
  end

  private

  def find_or_initialize_tags(tags)
    tags.uniq.compact.map do |tag|
      Tag.where(title: tag).first_or_initialize
    end
  end
end
