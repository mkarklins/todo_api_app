class Tag < ApplicationRecord
  validates :title, presence: true, length: { maximum: 255 }, uniqueness: true
end
