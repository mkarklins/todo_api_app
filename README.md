# README

Simple Todo app with json-api built with Ruby on Rails.

* Tested with ruby version: 2.4.2

* Getting started
  - `bundle install`
  - `rake db:create db:migrate`
  - `rails s`
