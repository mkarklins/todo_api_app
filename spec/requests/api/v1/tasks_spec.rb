require "rails_helper"

describe Api::V1::TasksController, type: :request do
  let(:json_headers) { { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' } }

  describe "index" do
    it "list all tasks" do
      Task.create!(title: "task_1", id: 1)
      Task.create!(title: "task_2", id: 2)

      get "/api/v1/tasks"

      serialized_tasks = %({
        "data":[
          {"id":"1", "type":"tasks", "attributes": {"title":"task_1"}, "relationships":{"tags": {"data": []}}},
          {"id":"2", "type":"tasks", "attributes": {"title":"task_2"}, "relationships":{"tags": {"data": []}}}
        ]})

      expect(response.body).to be_json_eql(serialized_tasks)
    end

    context "when there are no tasks" do
      it "returns empty data" do
        get "/api/v1/tasks"
        expect(response.body).to be_json_eql(%({"data": []}))
      end
    end
  end

  describe "create" do
    it "can create a new task" do
      task_body = %({
        "data":{
          "type":"undefined",
      		"id":"undefined",
      		"attributes":{ "title":"Do Homework" }
      	}})

      expected_response = %({"type":"tasks","attributes":{"title":"Do Homework"},"relationships":{"tags": {"data": []}}})

      expect{ post("/api/v1/tasks", params: task_body, headers: json_headers) }.to change(Task, :count).from(0).to(1)
      expect(response.body).to have_json_path("data/id")
      expect(response.body).to include_json(expected_response)
    end

    context "when passing invalid params" do
      it "outputs formatted error" do
        task_body = %({
          "data":{
            "type":"undefined",
        		"id":"undefined",
        		"attributes":{ "title":"" }
        	}})

        expected_response = %({
          "errors":[
            {"source":{"pointer":"/data/attributes/title"}, "detail":"can't be blank"}
          ]})

        expect{ post("/api/v1/tasks", params: task_body, headers: json_headers) }.to change(Task, :count).by(0)
        expect(response.body).to be_json_eql(expected_response)
      end
    end
  end

  describe "update" do
    it "can update an existing task" do
      task = Task.create!(title: "Do Homework")

      task_body = %({
        "data":{
          "type":"#{task.id}",
          "id":"tasks",
          "attributes":{ "title":"Watch a movie!", "tags": ["Urgent", "Home"] }
        }})

      put "/api/v1/tasks/#{task.id}", params: task_body,headers: json_headers

      expected_response = %({
        "type":"tasks",
        "attributes":{"title":"Watch a movie!"},
        "relationships":{"tags":{"data":[{"id":"1","type":"tags"},{"id":"2","type":"tags"}]}}
      })
      expect(response.body).to include_json(expected_response)
    end

    context "when passing invalid params" do
      it "outputs formatted error" do
        task = Task.create!(title: "Do Homework")

        task_body = %({
          "data":{
            "type":"#{task.id}",
            "id":"tasks",
            "attributes":{ "title":"" }
          }})

        put "/api/v1/tasks/#{task.id}", params: task_body,headers: json_headers

        expected_response = %({
          "errors":[
            {"source":{"pointer":"/data/attributes/title"}, "detail":"can't be blank"}
          ]})
        expect(response.body).to be_json_eql(expected_response)
      end
    end
  end

  describe "destroy" do
    it "is able to destroy a task" do
      resource = Task.create!(title: "Do Homework")
      expect{ delete "/api/v1/tasks/#{resource.id}", headers: json_headers }.to change(Task, :count).by(-1)
      expect(response.code).to eq "204"
    end
  end
end
