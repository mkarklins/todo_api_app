require "rails_helper"

describe Api::V1::TagsController, type: :request do
  let(:json_headers) { { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' } }
  let(:tags){ Tag.name.downcase.pluralize }

  describe "index" do
    it "list all tags" do
      Tag.create!(title: "tag_1", id: 1)
      Tag.create!(title: "tag_2", id: 2)

      get "/api/v1/tags"

      serialized_tags = %({
        "data":[
          {"id":"1", "type":"tags", "attributes": {"title":"tag_1"}},
          {"id":"2", "type":"tags", "attributes": {"title":"tag_2"}}
        ]})

      expect(response.body).to be_json_eql(serialized_tags)
    end

    context "when there are no tags" do
      it "returns empty data" do
        get "/api/v1/tags"
        expect(response.body).to be_json_eql(%({"data": []}))
      end
    end
  end

  describe "create" do
    it "can create a new tag" do
      tag_body = %({
        "data":{
          "type":"undefined",
          "id":"undefined",
          "attributes":{ "title":"Do Homework" }
        }})

      expected_response = %({"type":"tags","attributes":{"title":"Do Homework"}})

      expect{ post("/api/v1/tags", params: tag_body, headers: json_headers) }.to change(Tag, :count).from(0).to(1)
      expect(response.body).to have_json_path("data/id")
      expect(response.body).to include_json(expected_response)
    end

    context "when passing invalid params" do
      it "outputs formatted error" do
        tag_body = %({
          "data":{
            "type":"undefined",
            "id":"undefined",
            "attributes":{ "title":"" }
          }})

        expected_response = %({
          "errors":[
            {"source":{"pointer":"/data/attributes/title"}, "detail":"can't be blank"}
          ]})

        expect{ post("/api/v1/tags", params: tag_body, headers: json_headers) }.to change(Tag, :count).by(0)
        expect(response.body).to be_json_eql(expected_response)
      end
    end
  end

  describe "update" do
    it "can update an existing tag" do
      tag = Tag.create!(title: "Do Homework")

      tag_body = %({
        "data":{
          "type":"#{tag.id}",
          "id":"tags",
          "attributes":{ "title":"Watch a movie!" }
        }})

      put "/api/v1/tags/#{tag.id}", params: tag_body, headers: json_headers

      expected_response = %({"type":"tags","attributes":{"title":"Watch a movie!"}})
      expect(response.body).to include_json(expected_response)
      expect(tag.reload.title).to eq "Watch a movie!"
    end

    context "when passing invalid params" do
      it "outputs formatted error" do
        tag = Tag.create!(title: "Do Homework")

        tag_body = %({
          "data":{
            "type":"#{tag.id}",
            "id":"tags",
            "attributes":{ "title":"" }
          }})

        put "/api/v1/tags/#{tag.id}", params: tag_body, headers: json_headers

        expected_response = %({
          "errors":[
            {"source":{"pointer":"/data/attributes/title"}, "detail":"can't be blank"}
          ]})
        expect(response.body).to be_json_eql(expected_response)
      end
    end
  end

  describe "destroy" do
    it "is able to destroy a tag" do
      tag = Tag.create!(title: "Do Homework")
      expect{ delete "/api/v1/tags/#{tag.id}", headers: json_headers }.to change(Tag, :count).by(-1)
      expect(response.code).to eq "204"
    end
  end
end
