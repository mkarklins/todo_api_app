require "rails_helper"

describe Api::V1::TaskSerializer do
  let(:tags){ [Tag.new(title: 'some_tag')] }
  let(:task){ Task.new(title: 'task_1', id: 1, tags: tags) }

  subject { described_class.new(task) }

  it "correctly serializes a task" do
    serialized_task = %({"id":"1","title":"task_1","tags":[{"title": "some_tag"}]})
    expect(subject.to_json).to be_json_eql(serialized_task)
  end
end
