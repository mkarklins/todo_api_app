require "rails_helper"

describe Api::V1::TagSerializer do
  let(:tag){ Tag.new(title: 'tag_1', id: 1) }

  subject { described_class.new(tag) }

  it "correctly serializes a tag" do
    serialized_tag = %({"id":"1","title":"tag_1"})
    expect(subject.to_json).to be_json_eql(serialized_tag)
  end
end
