require "rails_helper"

describe Tag do
  describe "validations" do
    it "title is mandatory" do
      expect(Tag.new(title: "")).not_to be_valid
      expect(Tag.new(title: nil)).not_to be_valid
      expect(Tag.new(title: "example")).to be_valid
    end

    it "title cannot be longer than 255 chars" do
      expect(Tag.new(title: "a"*256)).not_to be_valid
      expect(Tag.new(title: "a"*255)).to be_valid
    end

    it "title must be unique" do
      Tag.new(title: "example").save!
      tag = Tag.new(title: "example")
      tag.valid?

      expect(tag.errors[:title].count).to eq 1
    end
  end
end
