require "rails_helper"

describe Task do
  describe "validations" do
    it "title is mandatory" do
      expect(Task.new(title: "")).not_to be_valid
      expect(Task.new(title: nil)).not_to be_valid
      expect(Task.new(title: "example")).to be_valid
    end

    it "title cannot be longer than 255 chars" do
      expect(Task.new(title: "a"*256)).not_to be_valid
      expect(Task.new(title: "a"*255)).to be_valid
    end
  end

  describe "associations" do
    it "has many tags" do
      task = Task.create!(title: "example")
      task.tags.create!(title: "example_tag")

      expect(task.tags.map(&:title)).to eq ["example_tag"]
    end
  end

  describe "#save_with_tags" do
    it "saves record with tags" do
      Tag.create!(title: 'tag_2')
      task = Task.new(title: "example")

      expect(task.save_with_tags(['tag_1', 'tag_2'])).to be true
      expect(task.tags.map(&:title)).to eq ['tag_1', 'tag_2']
    end

    it "does not create duplicates or empty tags" do
      task = Task.new(title: "example")
      task.save_with_tags(['tag_1', nil, 'tag_1'])

      expect(task.tags.map(&:title)).to eq ['tag_1']
    end
  end
end
